<?php
/**
 * @Author: chenkaijian <ikaijian@163.com>,
 * @Date: 2021/12/27 8:10 上午,
 * @LastEditTime: 2021/12/27 8:10 上午,
 * @Copyright: 2020 Ikaijian Inc. 保留所有权利。
 */

// 服务定义
$consumerServices = [
    'UserService' => \App\JsonRpc\UserServiceInterface::class,
];

return [
    'consumers' => value(function () use ($consumerServices) {
        $consumers = [];
        foreach ($consumerServices as $name => $interface) {
            $consumers[] = [
                'name' => $name,
                'service' => $interface,
                'nodes' => [
                    ['host' => '127.0.0.1', 'port' => 9600],
                ],
            ];
        }
        return $consumers;
    }),
];
