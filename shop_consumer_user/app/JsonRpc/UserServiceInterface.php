<?php
/**
 * @Author: chenkaijian <ikaijian@163.com>,
 * @Date: 2021/12/27 7:54 上午,
 * @LastEditTime: 2021/12/27 7:54 上午,
 * @Copyright: 2020 Ikaijian Inc. 保留所有权利。
 */

namespace App\JsonRpc;


interface UserServiceInterface
{
    public function createUser(string $name, int $gender);

    public function getUserInfo(int $id);
}