<?php
/**
 * @Author: chenkaijian <ikaijian@163.com>,
 * @Date: 2021/12/27 7:59 上午,
 * @LastEditTime: 2021/12/27 7:59 上午,
 * @Copyright: 2020 Ikaijian Inc. 保留所有权利。
 */

namespace App\Controller;

use App\JsonRpc\UserServiceInterface;

/**
 * Class UserController
 * @package App\Controller
 * @AutoController()
 */
class UserController extends AbstractController
{
    /**
     * @Inject()
     * @var UserServiceInterface
     */
    private $userServiceClient;

    /**
     *
     *
     * @return mixed
     * @Date: 2021/12/27 8:03 上午
     * @Author: ikaijian
     */
    public function createUser()
    {
        $name = (string)$this->request->input('name', '');
        $gender = (int)$this->request->input('gender', 0);
        return $this->userServiceClient->createUser($name, $gender);
    }

    /**
     *
     *
     * @return mixed
     * @Date: 2021/12/27 8:03 上午
     * @Author: ikaijian
     */
    public function getUserInfo()
    {
        $id = (int)$this->request->input('id');
        return $this->userServiceClient->getUserInfo($id);
    }
}