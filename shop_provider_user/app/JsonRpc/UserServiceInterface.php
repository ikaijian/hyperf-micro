<?php
/**
 * @Author: chenkaijian <ikaijian@163.com>,
 * @Date: 2021/11/29 1:16 上午,
 * @LastEditTime: 2021/11/29 1:16 上午,
 * @Copyright: 2020 Ikaijian Inc. 保留所有权利。
 */

namespace App\JsonRpc;


interface UserServiceInterface
{

    public function createUser(string $name, int $gender);

    public function getUserInfo(int $id);
}